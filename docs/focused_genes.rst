User-defined set of genes
^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /_static/T7.Focus.png

In cases where specific gene(s) need to be investigated, users can simply type in gene symbo(s) into text box (``User genes``).
CGAR will show any variants on the gene(s), provided that the variant matches other conditions as well.
Additionally, CGAR shows phenotype and variant category from HGMD\ |reg| (requires license) (columns ``HGMD phenotype`` and ``HGMD class variant``).

.. |reg| unicode:: U+000AE .. REGISTERED SIGN
