Genes associated with rare phenotype
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning:: The contents for this section is available only for users licensed from `OrphaData.org <http://www.orphadata.org/>`_.
   Please note that the registration to CGAR does not automatically grant access to data from OrphaData.org.
   Users need to obtain license from OrphaData.org, separately.

`OrphaData.org <http://www.orphadata.org>`_ provides large data sets related to rare disease such as genes related to rare diseases or epidemiological data.
CGAR uses these data to find variants on genes associated with rare disease phenotypes. Here, no additional input from users is required (other than common paramteres such as allele frequency).

.. image:: /_static/T3.Orpha.png

In the main table, 5 columns show informations on rare phenotype associated with the variant (through gene), to enhance variant interpretation.

* ``Orphanet disease``: the name and identifier of phenotype used in OrphaNet with link.
* ``Associated HPO disorder``: opens a table listing every `Human Phenotype Ontology <https://hpo.jax.org>`_ terms associated with the phenotype, a standardized terms for human disease abnomalities. Useful to correlate with other data using HPO terms.
* ``Age of onset & age of death``: age of onset and/or death for the phenotype. Could be used to further filter out irrelevant variants. For example, in a sample where the first symptom occured at 20s, phenotypes with age of onset as **Infancy** can be disregarded.
* ``Inheritance``: mode of inheritance for the phenotype. Also could be used to filter out irrelevant variants. If a subspected phenotype is **Autosomal recessive**, heterozygous variants can be less likely the target.
* ``Prevalence & location``: phenotype frequency by geological locations. Also could be used for further filtering variant, especially in combination with the ancestry of the sample.

