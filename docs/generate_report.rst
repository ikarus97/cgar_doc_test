Start Analysis on CGAR
----------------------

To start analysing any of samples (assuming they are ready), simply click ``Report`` from the top menu to open the dialog as follows.

.. seealso:: Refer to :ref:`upload` for how to add new samples or check if the samples are ready.

.. image:: /_static/select_sample.png
   :scale: 70 %
   :align: center

From the dropdown list at the top, users can select from list of analysis-ready samples.
The CGAR public server (`link <https://tom.tch.harvard.edu/apps/cgar/>`_) provides a list of publicly available samples for users who want to try out CGAR. See :ref:`public-samples`.

If available, users can specify parental samples for the selected case in ``Parental samples for de novo candidates``.
The dropdown lists ``Paternal sample`` and ``Maternal sample``, both also populated with analysis-ready samples, are used to specify paternal and maternal cases, respectively.
For the *de novo* candidate analysis to work, both ``Paternal sample`` and ``Maternal sample`` needs to be specified.

.. seealso:: Refer to :ref:`denovo` for more details on trio analysis using CGAR.

For analysis using :ref:`public-samples`, three short-cut links are provided for each of three trios (``Trio1 caucasian``, ``Trio2 Han Chinese``, and ``Trio3 Ashkenazim``).

In cases where users are interested in variants on genes associated with specific phenotypes, yet not certain about which genes to look for, they can provide a list of keywords describing the phenotype of interest in ``Phenotype(s) required for Phenotype associated tab``.
Users can type in as many keywords as they want, each separated by space.
CGAR will find any variants on genes associated with any of keywords.

.. seealso:: Check :ref:`findzebra` for more details on how the genes associated with the given keywords are used in CGAR.


For more focused analysis on variants in pre-defined set of genes, users can provide their genes of interest through ``Gene(s) required for Focused report``.
CGAR will expect a list of official gene symbols (HUGO symbol link...), each separated by either comma or space.
For the convenience, the list of cancer-associated genes as curated by `Cancer Gene Census <http://cancer.sanger.ac.uk/cosmic/census?tier=1>`_ is provided as short-cut link.

When a matched control for cancer sample is available, specifying it as ``Germline sample for Cancer tab`` would make CGAR to show somatic variants, instead of any variants, on :code:`Cancer` subsection.
Again, a short-cut for public example of uveal melanoma case (tumor and its matched blood) is provided for convenience (``Uveal melanoma``).



Finally, users can specify how to utilize population-specific allele frequencies in CGAR.

#. **Maximum across population**: Default choice. Limits an allele frequency of a variant to the highest value (most common) across all populations. Results in the most strict selection of rare variants.
#. **Ethnicity specific: Genotype predicted**: Limits an allele frequency of a variant to the value from a population of the same global ancestry as estimated from variants' genotypes.
#. **Ethnicity specific: Self-reported**: The same as the above, but uses the ancestry specified upon uploading the variants file. Not available if **Unknown/Unspecified** is chosen.

.. seealso:: Check :ref:`ancestry` for details on genotype-based ancestry prediction in CGAR.

After setting all the above options, click the button ``Generate report`` for CGAR to start generating report tables.


.. _public-samples:

Publicly available samples
^^^^^^^^^^^^^^^^^^^^^^^^^^

The following samples are readily available to all users in CGAR public server.

* Maternal trio of `CHPH/Utah pedigree 1463 <https://www.coriell.org/0/Sections/Collections/NIGMS/CEPHFamiliesDetail.aspx?PgId=441&fam=1463&>`_

  * Mother (NA12878) as **trio1_NA12878_daughter**.
  * Maternal grandfather (NA12891) as **trio1_NA12891_father**.
  * Maternal grandmother (NA12892) as **trio1_NA12892_mother**.

* The Han Chinese trio from `Genome in a Bottle Consortium <http://www.genomeinabottle.org/>`_.

  * The son (NA24631) as **trio2_GM24631_son**.
  * The father (NA24694) as **trio2_GM24694_father**.
  * The mother (NA24695) as **trio2_GM24695_mother**.

* The Ashkenazim trio from `Genome in a Bottle Consortium <http://www.genomeinabottle.org/>`_.

  * The son (NA24385) as **trio3_NA24385_son**.
  * The father (NA24149) as **trio3_NA24149_father**.
  * The mother (NA24143) as **trio3_NA24143_mother**.

* A pair of tumor sample with its matched blood control from uveal melanoma (MIM: `155720 <https://www.omim.org/entry/155720>`_) case `ref <http://>`_.

  * Tumor sample as **patient1_cancer_melanoma**.
  * Control sample as **patient1_blood_melanoma**.

* Artifical examples of specific phenotypes.

  * Artificial sample for Miller syndrome (MIM: `263750 <https://www.omim.org/entry/263750>`_) as **Miller**.
  * Artificial sample for Schinzel-Giedion syndrome (MIM `269150 <https://www.omim.org/entry/269150>`_) as **schinzel_giedion**.

