Inside CGAR Report
------------------

For each sample, the generated report shows the name (identifier) of the chosen sample with 9 sections of variants organized by their analytical implications.

* (Restricted) Disease-associated variants reported in `HGMD <http://www.hgmd.cf.ac.uk/>`_ - ``HGMD``
* Disease-associated variants reported in `ClinVar <https://www.ncbi.nlm.nih.gov/clinvar/>`_ - ``ClinVar``
* (Restricted) Variants on genes associated with rare-diseases in `OrphaData <http://www.orphadata.org/>`_ - ``Orphanet``
* Variants on genes with putative association to user-defined phenotype - ``Phenotype associated``
* Secondary findings - ``ACMG59``
* Variants with potential pharmacogenomic implications - ``Pharmacogenomics``
* Variants on user-defined set of genes - ``Focused report``
* De novo variant candidates from trio - ``De novo candidates``
* (Somatic) Variants potentially associated with cancer - ``Cancer``

.. image:: /_static/report_example.png

The side manu on the left reflects settings for current section and provides means to change settings and to reanalyze.
The main table on the right presents the essential informations on variants as well as links to further details or external resources.

In the following sections, each component and section in CGAR report is described in detail.

.. toctree::
   ancestry
   report_tables
   hgmd
   clinvar
   orphadata
   pheno_genes
   acmg
   pgx
   focused_genes
   de_novo
   cancer

