.. _denovo:

De novo variant candidates
^^^^^^^^^^^^^^^^^^^^^^^^^^

In cases where variants from both parents are available with an index case, CGAR supports simple *de novo* variant analysis by identifying heterozygous variants from index case which was not found in both parents.

.. image:: /_static/T8.Denovo.png

On the side menu, index case (if different from current one) can be selected from ``Report for:`` dropdown list. Parental samples are specified by ``Paternal sample`` and ``Maternal sample``, respectively.

On the resulting table, if the *de novo* variant candidate is also found in `denovo-db <http://denovo-db.gs.washington.edu/>`_, a collection of germline *de novo* variants identified in human, the phenotype from denovo-db (``Phenotype``, the phenotype for the original family study) is also shown.
Additionally, CGAR shows phenotype and variant category from HGMD\ |reg| (requires license) (columns ``HGMD phenotype`` and ``HGMD class variant``).

.. |reg| unicode:: U+000AE .. REGISTERED SIGN
