Cancer-associated genes and variants
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``Cancer`` tab in CGAR reports variants that could have implications in cancer, based on the following categories:

1. Variants previously found in the `Catalog of Somatic Mutations in Cancer <https://cancer.sanger.ac.uk/cosmic>`_ (COSMIC), an expert-curated database of somatic mutations.

COSMIC maintains large collection of expert-curated somatic mutation information relating to human cancers.
From user sample, CGAR lists all variants that had matches in COSMIC.
If the sample is from cancer case, the variants previously found in COSMIC may worth further investigation, especially when the cancer types in COSMIC matches with the sample.

2. Variants on genes listed in `Cancer Gene Census <https://cancer.sanger.ac.uk/census>`_ (CGC), genes with mutations causally implicated in cancer.

CGC tries to catalog genes with mutations that have been causally implicated in cancer.
Genes in CGC are divided into two groups based on the level of documented evidence for oncogenic functionality of genes.
CGAR list variants on genes which are grouped as tier 1 in CGC: genes with documented activity relevant to cancer, evidence of mutations in cancer which can change the activity of gene product towards promoting oncogenic transformation.


Along with the above categories, if a matched control sample is avaialble, any variants that were found in both control and the case will be excluded.

.. image:: /_static/T9.Cancer.png

Checkboxes under ``Cancer`` on the side menu allows users to select how to identify variants with potential association with cancer (multiple choices are allowed).

The ``Cancer control`` dropdown list under the main sample ``Report for:`` allows users to specify the matched control sample.
