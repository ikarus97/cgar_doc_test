Installing CGAR locally
-----------------------

Requirements
^^^^^^^^^^^^

* `Docker <https://www.docker.com/>`_: any versions of Docker that support docker compose file version 2.
* Docker image file for CGAR: please contact `us <gnome.pipeline@gmail.com>`_ for getting the file.
* Free disk space of 700MB


Installation
^^^^^^^^^^^^

1. Import docker images for CGAR into local system:

.. code-block:: bash

   docker import [image file]


2. Install and prepare annotation files used in CGAR.

.. code-block:: bash

   sudo
